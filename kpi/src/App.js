import React from 'react';
import MainPage from './Pages/MainPage';
import WorkerPage from './Pages/WorkerPage/WorkerPage';
import ContentPage from './Pages/src/ContentPage/ContentPage'
import { BrowserRouter, Switch, Route } from 'react-router-dom';

const App = () => {
    return (
        <BrowserRouter>
        
            <Switch>
                <Route exact path="/"                render={() => <MainPage />     } /> 
                <Route exact path="/workerpage"      render={() => <WorkerPage />     } /> 
                <Route exact path="/contentpage"     render={() => <ContentPage />     } /> 
           </Switch>
        </BrowserRouter>
    );
}

export default App;
