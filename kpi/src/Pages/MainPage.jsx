import React from 'react';

import './MainPage.css';
import icon from './src/icons.png';

const MainPage = () => {
    return (

        <div className="c-mainpage">
            <div className="c-mainpage-left_menu">
            <div className="c-mainpage-left_menu-icons">
                <img src={icon} alt="icon" className="icon"/>
                <img src={icon} alt="icon" className="icon"/>
                <img src={icon} alt="icon" className="icon"/>
                <img src={icon} alt="icon" className="icon"/>
            </div>
            </div>
            <div className="c-mainpage-navigation">
                <div className="c-mainpage-navigation-wrapper">
                    <p className="margin">Name / Схемы</p>
                    <h1 className="c-mainpage-navigation-title margin">ЛЮДИ</h1>
                    <p className="c-mainpage-navigation-paragraph bold margin">Все</p>
                    <button className="c-mainpage-add-dep margin">+ Добавить отдел</button>
                </div>
            </div>
        </div>
    )
}
export default MainPage;